package sda;

public class Stack {
    private Node first = null;
    public boolean isEmpty(){
        return first == null;
    }
    public String Pop(){
        if(isEmpty()) 
            throw new RuntimeException("Can't Pop from an empty Stack.");
        else{
            String temp = first.value;
            first = first.next;
            return temp;
        }
    }
    public void Push(String o){
        first = new Node(o, first);
    }
    class Node{
        public Node next;
        public String value;
        public Node(String value){
            this(value, null); 
        }
        public Node(String value, Node next){
            this.next = next;
            this.value = value;
        }
    }
}
