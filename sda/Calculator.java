package sda;

import java.util.Arrays;
import java.util.List;

public class Calculator {
	static int cnt=0;
//	public static void RPN(String string, List<String> content) {
//		content.clear();
//		switch(cnt++) {
//			case 0: 
//				content.addAll(Arrays.asList(new String[] {"+", "2", "2"}));
//				break;
//			default: 
//				content.addAll(Arrays.asList(new String[] {"4"}));
//				break;
//		}
//	}
	
	public static String RPN (String exp, List<String> content){
		String outcome = "";
		String[] tokens = exp.split(" ");
		Stack stack = new Stack();
		for(int i = 0; i<tokens.length; i++){
			stack.Push(tokens[i]);
			content.add(tokens[i]);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(tokens[i].equals("+")){
				stack.Pop();
				content.remove(content.size()-1);
				Integer a = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer b = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer c = a + b;
			stack.Push(c.toString());
			content.add(c.toString());}
			else if(tokens[i].equals("*")){
				stack.Pop();
				content.remove(content.size()-1);
				Integer a = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer b = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer c = a * b;
			stack.Push(c.toString());
			content.add(c.toString());
			}
			else if(tokens[i].equals("-")){
				stack.Pop();
				content.remove(content.size()-1);
				Integer a = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer b = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer c = b - a;
			stack.Push(c.toString());
			content.add(c.toString());
			}
			else if(tokens[i].equals("/")){
				stack.Pop();
				content.remove(content.size()-1);
				Integer a = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer b = Integer.parseInt(stack.Pop());
				content.remove(content.size()-1);
				Integer c = b/a;
			stack.Push(c.toString());
			content.add(c.toString());
			}
			
		}
		outcome = stack.Pop();
		return outcome;
	}

}
